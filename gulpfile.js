var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var gulpConfiguration = require('./src/config/gulpconfig');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var minicss = require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');
var webserver = require('gulp-webserver');
var inject = require('gulp-inject');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var babel = require("gulp-babel");
var uglify = require('gulp-uglify');
var ts = require('gulp-typescript');
var plumber = require('gulp-plumber');


gulp.task('less', function() {
    var target = gulp.src(gulpConfiguration.Files.IndexFile);
    var sources = gulp.src(gulpConfiguration.Files.CSSFiles, { read: false });

    gulp.src(gulpConfiguration.Files.LessCompileFiles)
        .pipe(plumber({}))
        .pipe(sourcemaps.init())
        .pipe(less({}))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(gulpConfiguration.Files.CSSRoot));

    target.pipe(inject(sources, { relative: true }))
        .pipe(gulp.dest(gulpConfiguration.Files.AppRoot));
});

gulp.task('webServerDev', function() {
    gulp.src(['./app/'])
        .pipe(webserver({
            livereload: {
                enable: true
            },
            path: '/',
            directoryListing: false,
            open: true,
            host: 'localhost',
            https: false,
            port: 5610,
            fallback: 'index.html'

        }));
});

gulp.task('typescript', function() {

    var target = gulp.src(gulpConfiguration.Files.IndexFile);
    var sources = gulp.src(gulpConfiguration.Files.TypeScriptAppRoot, { read: false });

    gulp.src(gulpConfiguration.Files.TypeScriptFiles)
        .pipe(plumber({}))
        .pipe(ts({
            "target": "es5",
            "module": "system",
            "moduleResolution": "node",
            "sourceMap": true,
            "emitDecoratorMetadata": true,
            "experimentalDecorators": true,
            "removeComments": false,
            "noImplicitAny": false
        }))
        .pipe(gulp.dest(gulpConfiguration.Files.TypeScriptAppRoot));

    // Inject into HTML Page
    target.pipe(inject(sources, { relative: true }))
        .pipe(gulp.dest(gulpConfiguration.Files.AppRoot));
});

gulp.task('watch', function() {

    gulp.watch([gulpConfiguration.Files.LessFiles], ['less']);
    gulp.watch([gulpConfiguration.Files.TypeScriptFiles], ['typescript']);
    gulp.watch([gulpConfiguration.Files.HtmlFiles], ['less']);

});

gulp.task('default', ['webServerDev', 'watch', 'less', 'typescript']); 