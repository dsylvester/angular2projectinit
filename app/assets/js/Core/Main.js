System.register(['angular2/platform/browser', './CoreComponent'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var browser_1, CoreComponent_1;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (CoreComponent_1_1) {
                CoreComponent_1 = CoreComponent_1_1;
            }],
        execute: function() {
            browser_1.bootstrap(CoreComponent_1.AppComponent);
        }
    }
});
